# Press the green button in the gutter to run the script.
import csv

if __name__ == '__main__':

    with open('customer.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                #print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
                print(f'\t{row}')
                line_count += 1
        print(f'Processed {line_count} lines.')